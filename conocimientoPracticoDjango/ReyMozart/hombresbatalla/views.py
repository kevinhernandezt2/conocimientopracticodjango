from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm 
from hombresbatalla.forms import RegisterForm, RegisterUserArmForm
from django.contrib import messages 
from django.contrib.auth import authenticate, login, logout 
from hombresbatalla.models import  HombresbatallaHabilidad, HombresbatallaArmamento, HombresbatallaUsuarioArmas, AuthUser
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

# Create your views here.

# metodo de Inicio
def index(request):

    return render(request, 'mainapp/index.html', {
        'title': 'Inicio'
    })

#metodo registrar pagina usuario soldado
def register_page(request):
    
    if request.user.is_authenticated:
        return redirect('inicio')
    else:

        register_form = RegisterForm() 
        
        if request.method == 'POST':
            register_form = RegisterForm(request.POST)

            if register_form.is_valid():
                register_form.save()
                messages.success(request, 'Te Registro ha sido Exitoso Soldado') 

                return redirect('inicio')

        return render(request, 'users/register.html', {
            'title': 'Registro Soldado',
            'registrar_formulario': register_form
        })

#metodo login usuario soldado
def login_page(request):

    if request.user.is_authenticated:
        return redirect('inicio')
    else:
        
        if request.method == 'POST':
            
            
            nombre_usuario = request.POST.get("username")
            contrasena = request.POST.get("password")

            usuario = authenticate(request, username=nombre_usuario, password=contrasena)

            if usuario != None:
                login(request, usuario)
                messages.success(request, f"Bienvenido Soldado {nombre_usuario}")
                return redirect('inicio')
            
            else:
                messages.warning(request, 'No te has identificado correctamente Soldado :(')

        return render(request, 'users/login.html', {
            'titleLogin': 'Identificate Soldado'
        })

#metodo cierre de sesion
def logout_user(request):
    logout(request) 
    return redirect('/login')

#metodo para renderizar seleccion desplegable de habilidades y armamentos
def list_arm_hab(request):
    
    consultaHabilidad = HombresbatallaHabilidad.objects.all()
    consultaArmamento = HombresbatallaArmamento.objects.all()

    return render(request, 'armaments/soldadoRegister.html', {
        'listHabilidades': consultaHabilidad,
        'listArmamentos': consultaArmamento
    })

#metodo registrar o save armamento soldado
@login_required(login_url="logueo")
def register_sold(request):
    
    #aqui iba a utilizar del archivo form.py donde personalizo formularios
    """
    register_user_arm_form = RegisterUserArmForm() 

    return render(request, 'armaments/soldadoRegister.html', {
        'registrarArmaFormulario': register_user_arm_form
    })
    
    """
    #se utiliza el llamado POST mediante html y no formulario personalizado

    if request.method == 'POST':

        c = request.POST['codigo']
        h = request.POST['habilidad']
        a = request.POST['armamento']
        
        au = User(
        id = c
        )
        
        ha = HombresbatallaHabilidad(
        id_habilidad = h
        )

        arm = HombresbatallaArmamento(
        id_armamento = a
        )
        
        userArmas = HombresbatallaUsuarioArmas(
        user = au,
        armamento = arm,
        habilidad = ha
        
        )

        if (userArmas.user == "" or userArmas.user == None or userArmas.user == 0)  and userArmas.armamento == 0 and userArmas.habilidad == 0:
            messages.error(request, "Error seleccionar algun campo o campos vacios")
            return redirect('registerA')
        elif userArmas.user == "" or userArmas.user == None:
            messages.error(request, "campos vacio usuario")
            return redirect('registerA')
        elif userArmas.habilidad == 0:
            messages.error(request, "Error seleccione algun campo para habilidad")
            return redirect('registerA')
        elif userArmas.armamento == 0:
            messages.error(request, "Error seleccione algun campo para el armamento")
            return redirect('registerA')

        userArmas.save()
        messages.success(request, "SOLDADO SE HA REGISTRADO SU ARMAMENTO")

        return redirect('registerA')
        
    else:
            
        messages.error(request, "HUBO ALGUN ERROR SOLDADO")
        return redirect('registerA')

#metodo para historial armamento utilizando subconsulta
@login_required(login_url="logueo")
def list_id_sold(request, codigo):

    
    consulta_soldado_id = HombresbatallaUsuarioArmas.objects.filter(user = codigo).values_list('user', flat=True)
    consulta_soldado_armamento = HombresbatallaUsuarioArmas.objects.filter(user = codigo).values_list('armamento', flat=True)
    
    sold_id_arm = HombresbatallaArmamento.objects.filter(id_armamento__in=consulta_soldado_armamento).values_list('nombre_arma')

    return render(request, 'armaments/historyArm.html', {
        'listIdSoldado': consulta_soldado_id,
        'armas': sold_id_arm
    })

#metodo de listado de usuario por id para que traiga los armamnetos registrados para editar o eliminar
@login_required(login_url="logueo")
def change_id_sold(request, id):

    consulta_soldado_id = HombresbatallaUsuarioArmas.objects.filter(user = id)

    return render(request, 'armaments/editSoldArms.html', {
        'listIdSoldado': consulta_soldado_id
    })

#metodo editar o cambiar armamneto de soldado
@login_required(login_url="logueo")
def edit_soldarm_id(request, cod):

    
    cons_pk_tbuserarm = HombresbatallaUsuarioArmas.objects.get(pk=cod)
    formu = RegisterUserArmForm(instance=cons_pk_tbuserarm)

    if request.method == 'POST':

        formu = RegisterUserArmForm(request.POST, instance=cons_pk_tbuserarm)

        if formu.is_valid():
            
            formu.save()
            messages.success(request, "SOLDADO SE HA EDITADO SU ARMAMENTO")

            return redirect('registerA')


    return render(request, 'armaments/soldadoRegister.html', {
        'searchEditArm': formu
    })

#metodo eliminar armamento para repetidos o si ya no lo desea o cualquier otra eventualidad
@login_required(login_url="logueo")
def delete_arm_sold(request, codigoId):
    
    hb_user_arms = HombresbatallaUsuarioArmas.objects.get(id_usuario_armas=codigoId)

    hb_user_arms.delete()
    messages.warning(request, "SE HA ELIMINADO ARMAMENTO SOLDADO")

    return redirect('registerA')
    

