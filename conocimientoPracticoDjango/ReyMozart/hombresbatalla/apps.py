from django.apps import AppConfig


class HombresbatallaConfig(AppConfig):
    name = 'hombresbatalla'
    verbose_name = 'Gestion Hombres de Batalla'
