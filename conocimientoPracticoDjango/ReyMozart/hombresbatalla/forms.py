from django import forms 
from django.core import validators 

from django.contrib.auth.forms import UserCreationForm 
from django.contrib.auth.models import User 

from django.forms import ModelForm
from hombresbatalla.models import HombresbatallaUsuarioArmas

#se crea archivo forms.py para formulario de registro personalizado
class RegisterForm(UserCreationForm):
    
    class Meta:
        model = User 
        fields = ['username', 'first_name', 'last_name', 'password1', 'password2']

class RegisterUserArmForm(ModelForm):

    class Meta:
        model = HombresbatallaUsuarioArmas
        fields = ['user', 'habilidad', 'armamento']