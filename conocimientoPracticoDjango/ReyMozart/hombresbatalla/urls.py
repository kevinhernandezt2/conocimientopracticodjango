from django.urls import path
from . import views 

urlpatterns = [
    path('', views.index, name="index"),
    path('inicio/', views.index, name="inicio"),
    path('registro/', views.register_page, name="register"),
    path('login/', views.login_page, name="logueo"),
    path('cierreSesion/', views.logout_user, name="logout"),
    path('registroArmamento/', views.list_arm_hab, name="registerA"),
    path('save-armament/', views.register_sold, name='save'),
    path('consult-sold-arm/<int:codigo>', views.list_id_sold, name='listArmSol'),
    path('edit-sold-arm/<int:id>', views.change_id_sold, name='editArmsol'),
    path('update-armament/<int:cod>', views.edit_soldarm_id, name='updateArmSol'),
    path('delete-armament/<int:codigoId>', views.delete_arm_sold, name="deleteArmSol")
]
