from django.contrib import admin
from.models import HombresbatallaArmamento, HombresbatallaHabilidad

# Register your models here.

admin.site.register(HombresbatallaArmamento,)
admin.site.register(HombresbatallaHabilidad,)

title = "Proyecto Mozart"
subtitle = "Panel gestion"

admin.site.site_header = title
admin.site.site_title = title
admin.site.index_title = subtitle

